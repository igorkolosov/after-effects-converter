import React, { Component } from 'react';
import './App.css';


const moooovieJson = {};

function makePhpObjects(props, duration) {

  const getJson = props;
  let firstJson = {};

  // HEADER
  firstJson['Header'] = getJson['Header'];

  // MAIN
  firstJson['Main'] = [];
  let currentMusicLayerCounter = 1;
  let currentFootageLayerCounter = 1;
  let currentTextLayerCounter = 1;
  let currentValuesLayerCounter = 1;

  for (var i = 0; i < getJson['Main'].length; i++) {

    if (String(getJson['Main'][i]['Title']).toLowerCase() === 'footage') {

      if (String(getJson['Main'][i]['Name']).toLowerCase().substr(-4) === '.mp3') {

        const currentLayer = getJson['Main'][i];
        currentLayer['Path'] = "'.$this->music" + currentMusicLayerCounter + ".'";
        firstJson['Main'][i] = currentLayer;
        currentMusicLayerCounter += 1;

      } else {

        const currentLayer = getJson['Main'][i];
        currentLayer['Path'] = "'.$this->footage" + currentFootageLayerCounter + ".'";
        firstJson['Main'][i] = currentLayer;
        currentFootageLayerCounter += 1;

      } 

    } else if (String(getJson['Main'][i]['Title']) === 'Composition') {

      const currentLayer = getJson['Main'][i];
      firstJson['Main'][i] = currentLayer;

    } else if (String(getJson['Main'][i]['Title']) === 'Text') {
      
      const currentLayer = getJson['Main'][i];
      currentLayer['Values'] = "'.$this->text" + currentTextLayerCounter + ".'";
      firstJson['Main'][i] = currentLayer;
      currentTextLayerCounter += 1;

    } else if (String(getJson['Main'][i]['Title']) === 'Values') {
      
      const currentLayer = getJson['Main'][i];
      currentLayer['Values'] = ["'.$this->fon" + currentValuesLayerCounter + "_r.'", "'.$this->fon" + currentValuesLayerCounter + "_g.'", "'.$this->fon" + currentValuesLayerCounter + "_b.'"];
      firstJson['Main'][i] = currentLayer;
      currentValuesLayerCounter += 1;

    }

  }

  // RESULT

  let firstJsonString = "<?php\n\n$content = '\n\n\t";

  firstJsonString += JSON.stringify(firstJson, null, '\t');

  firstJsonString += "';\n\n$img = ',\n\t\"Renders\": [{\n\t\t\"Comp\": \"RENDER 1280x720\",\n\t\t\"CompID\": \"'.$this->comp.'\",\n\t\t\"RenderSettings\": \"Default\",\n\t\t\"OutputModules\": [{\n\t\t\t\"OMName\": \"JPEG\",\n\t\t\t\"Path\": \"'.$this->output_file.'\"\n\t\t}]\n\t}],\n\t\"Options\": {\n\t\t\"SaveProject\": \"Off\",\n\t\t\"RenderControl\": 0,\n\t\t\"Path\": \"undefined\",\n\t\t\"RenderProject\": true\n\t}\n}';\n\n$mov =  ',\n\t\"Renders\": [{\n\t\t\"Comp\": \"RENDER_PREVIEW\",\n\t\t\"CompID\": 12,\n\t\t\"Start\": 0,\n\t\t\"Duration\": ";

  firstJsonString += duration;

  firstJsonString += ",\n\t\t\"RenderSettings\": \"Mooofix\", \n\t\t\"OutputModules\":[{\n\t\t\t\"OMName\": \"qt-h264\",\n\t\t\t\"Path\": \"'.$this->output_file.'\"\n\t\t}]\n\t}],\n\t\"Options\": {\n\t\t\"SaveProject\": \"To_Path\",\n\t\t\"RenderControl\": 1,\n\t\t\"Path\": \"'.$this->output_file.'_apiproject_.aep\",\n\t\t\"RenderProject\": true\n\t}\n}';\n\n $movhd = ',\n\t\"Renders\": [{\n\t\t\"Comp\": \"MAIN RENDER\",\n\t\t\"CompID\": 1,\n\t\t\"Start\": 0,\n\t\t\"Duration\": ";

  firstJsonString += duration;

  firstJsonString += ",\n\t\t\"RenderSettings\": \"Mooo\",\n\t\t\"OutputModules\": [{\n\t\t\t\"OMName\": \"qt-h264\",\n\t\t\t\"Path\": \"'.$this->output_file.'\"\n\t\t}]\n\t}],\n\t\"Options\": {\n\t\t\"SaveProject\": \"To_Path\",\n\t\t\"RenderControl\": 1,\n\t\t\"Path\": \"'.$this->output_file.'_apiproject_.aep\",\n\t\t\"RenderProject\": true\n\t}\n}';\n\n?>"

  // RESULT
  return firstJsonString

}



function makeScenesComponents(props) {
  const getJson = props;
  let scenesComponents = {};

  if (Object.keys(props).length === 0 && props.constructor === Object) {
    return scenesComponents
  } else {
    for (var i = 0; i < getJson['Main'].length; i ++) {
      if (getJson['Main'][i]['Title'] === 'Composition' && String(getJson['Main'][i]['Comp']).substr(0,8) === 'PREVIEW_' && Number(getJson['Main'][i]['CompID']) !== 243) {
        scenesComponents[getJson['Main'][i]['CompID']] = [1, 1];
      }
    }
    return scenesComponents
  }
  
}



function makeScenes(props, scenesList) {

  const getJson = props;
  let picsCounter = 1;
  let textsCounter = 1;
  let textsLayersCounter = 0;

  for (var i = 0; i < getJson['Main'].length; i ++) {
    if (getJson['Main'][i]['Title'] === 'Text') {
      textsLayersCounter = i;
      break;
    }
  }

  // INITIAL:
  let scenesJson = "<?php\n\n$vars = array(\n"

  // CONVERTED:
  let scenesCompositions = [];

  for (var j = 0; j < getJson['Main'].length; j ++) {
    if (getJson['Main'][j]['Title'] === 'Composition' && String(getJson['Main'][j]['Comp']).substr(0,8) === 'PREVIEW_' && Number(getJson['Main'][j]['CompID']) !== 243) {
      scenesCompositions.push(getJson['Main'][j]);
    }
  }

  for (var k = 0; k < scenesCompositions.length; k ++) {

    let sceneArray = "\t'" + scenesCompositions[k]['CompID'] + "' => array(";
    const picsNumber = scenesList[scenesCompositions[k]['CompID']][0];
    const textsNumber = scenesList[scenesCompositions[k]['CompID']][1];

    // PICTURES
    for (let j = 0; j < picsNumber; j++) {
      let fileName = '';
      if (getJson['Main'][picsCounter]) {
        if (getJson['Main'][picsCounter]['Path']) {
                  fileName = getJson['Main'][picsCounter]['Path'].replace(/^.*[\\/]/, '');
                  sceneArray += "\n\t\t'footage" + Number(picsCounter) + "' => array('type' => 'media', 'width' => 1000, 'default' => '" + fileName + "'),";        
        }
      }
      picsCounter += 1;
    }

    // TEXTS
    for (let j = 0; j < textsNumber; j++) {

      let fileName = '';
      if (getJson['Main'][textsLayersCounter]) {
        if (getJson['Main'][textsLayersCounter]['Title'] === 'Text') {
            fileName = getJson['Main'][textsLayersCounter]['Values'];
            sceneArray += "\n\t\t'text" + Number(textsCounter) + "' => array('type' => 'text', 'default' => '" + fileName + "'),";        
        }
      }
      textsCounter += 1;
      textsLayersCounter += 1;

    }

    sceneArray += "\n\t),\n";
    scenesJson += sceneArray;

  }

  for (var n = 0; n < getJson['Main'].length; n ++) {
    if (Number(getJson['Main'][n]['CompID']) === 243) {
      // COLORS
      scenesJson += "\t'243' => array(";
      let colorsCounter = 1;
      for (var m = 0; m < getJson['Main'].length; m ++) {

        if (getJson['Main'][i]['Title'] === 'Values') {
          let firstValue = (getJson['Main'][m]['Values'][0]*255).toFixed(0);
          let secondValue = (getJson['Main'][m]['Values'][1]*255).toFixed(0);
          let thirdValue = (getJson['Main'][m]['Values'][2]*255).toFixed(0);
          function componentToHex(c) {
              var hex = c.toString(16);
              return hex.length == 1 ? "0" + hex : hex;
          }
          function rgbToHex(r, g, b) {
              return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
          }
          scenesJson += "\n\t\t'fon" + Number(colorsCounter) + "' => array( 'type' => 'hexcolor', 'default' => '" + rgbToHex(Number(firstValue), Number(secondValue), Number(thirdValue)) + "'),";            
          colorsCounter += 1;
        }

      }

      scenesJson += "\n\t),";

    }
  }

  // MUSIC
  scenesJson += "\n\t'main' => array(\n\t\t'music1' => array(\n\t\t\t'type' => 'media', \n\t\t\t'default' => '";
  scenesJson += String(getJson['Main'][0]['Name']);
  scenesJson += "'\n\t\t)\n\t)";

  // END:
  scenesJson += "\n);\n\nprint_r(json_encode($vars, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));\n\n?>"

  return scenesJson;

}



class ScenesInterface extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {};
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {

    const targetId = event.target.id.split('-')[1];
    const targetType = event.target.id.split('-')[0];
    let scenesComponents = this.props;

    for (var scene in scenesComponents.components) {      
      if (Number(scene) === Number(targetId)) {
        if (targetType === 'pics') {
          const prevValueTexts = scenesComponents.components[scene][1];
          scenesComponents.components[scene] = [Number(event.target.value), prevValueTexts];
        } else {
          const prevValuePics = scenesComponents.components[scene][0];
          scenesComponents.components[scene] = [prevValuePics, Number(event.target.value)];
        }        
      }
      this.setState({ scenesComponents: scenesComponents.components });
    }

  }

  render() {
    const scenesIdsList = this.props.components;

    var result = Object.keys(scenesIdsList).map(function(key) {
      if (Number(key) === 243) {
        return [Number(key), scenesIdsList[key]];
      } else {
        return [Number(key), scenesIdsList[key]];
      }
    });

    const listItems = result.map((number) =>
      <div>
        <h3>{number[0]}</h3>
        <div>
          <div>
            <p>Картинки:</p>
            <select value={number[1][0]} id={'pics-'+number[0]} key={'pics-'+number[0]}>
              <option>0</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
            </select>
          </div>
          <div>
            <p>Тексты:</p>
            <select value={number[1][1]} id={'texts-'+number[0]} key={'texts-'+number[0]}>
              <option>0</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
            </select>
          </div>
        </div>    
      </div>
    );

    if (Object.keys(this.props.components).length === 0 && this.props.components.constructor === Object) {
      return (
        <div className="selects-list-wrapper">
          <h2 className="selects-list-title">No JSON no scenes :(</h2>
        </div>
      )
    } else {
      return (
        <div className="selects-list-wrapper" onChange={this.handleChange}>
          <h2 className="selects-list-title">Scenes:</h2> 
          <div className="selects-list">
            { listItems }
          </div>
        </div>
      )
    }

  }
}


class App extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      value: JSON.stringify(moooovieJson, null, '\t'),
      convertedJson: '',
      convertedScenes: '',
      valueDuration: 40,
      scenesCompositions: [],
      scenesComponents: makeScenesComponents(moooovieJson),
      newComp: '',
      isJsonValid: false,
    };

    this.onInputDurationChange = this.onInputDurationChange.bind(this);
    this.onConvertClick = this.onConvertClick.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event) {
    this.setState({ value: event.target.value });
    function parseValidJson(str) {
        try {
            return JSON.parse(str);
        } catch (e) {
            // console.error(e.message);
            return false;
        }
    }
    if (!parseValidJson(event.target.value)) {
      this.setState({ isJsonValid: false });
    } else if (String(event.target.value) === '') {
      this.setState({ isJsonValid: false });
    } else {
      this.setState({ scenesComponents: makeScenesComponents(JSON.parse(event.target.value)), isJsonValid: true });
    }
  }

  onInputDurationChange(event) {
    this.setState({ valueDuration: event.target.value });
  }

  onConvertClick(event) {
    const getJson = JSON.parse(this.state.value);
    this.setState({ convertedJson: makePhpObjects(getJson, this.state.valueDuration) });
    this.setState({ convertedScenes: makeScenes(JSON.parse(this.state.value), this.state.scenesComponents) });
  }

  render() {
    let buttonText = '';
    if (this.state.isJsonValid) {
      buttonText = 'КОНВЕРТИРОВАТЬ'
    } else {
      buttonText = 'JSON не валидный :('
    }

    return (
      <div className="App">
        <div className="workarea">
          <div className="textareas">
            <div className="textarea-getjson">
              <h2>JSON FROM AFTER EFFECTS:</h2>
              <textarea 
                value={this.state.value}
                onChange={this.onInputChange} ></textarea>
            </div>
            <div className="textarea-firstjson">
              <h2>GENERATED PHP OBJECTS:</h2>
              <textarea 
                value={this.state.convertedJson}
                readOnly ></textarea>
            </div>
            <div className="textarea-scenes">
              <h2>GENERATED PHP SCENES:</h2>
              <textarea 
                value={this.state.convertedScenes}
                readOnly ></textarea>
            </div>
          </div>
          <div className="controls">
            <div className="controls-interface" style={{display: this.state.isJsonValid ? 'block' : 'none' }}>
              <div className="textarea-getjson-controls">
                <h2>Duration: </h2>
                <input 
                  className='textarea-getjson-duration'
                  type='text'
                  value={this.state.valueDuration}
                  onChange={this.onInputDurationChange}
                />
              </div>
              <ScenesInterface components={this.state.scenesComponents} style={{display: this.state.isJsonValid ? 'block' : 'none' }} />
            </div>
            <button className="btnConvert" onClick={this.onConvertClick} disabled={!this.state.isJsonValid}>
             { buttonText }
            </button>
          </div>
        </div>
      </div>
    );
  }
}


export default App;

